const express = require('express')
const app = express()
const port = 3000

/**
 * This function adds two numbers together
 * @param {Number} a 
 * @param {Number} b 
 * @returns {Number}
 */
const add = (a, b) => {
  return a + b;
}

app.get('/add/', (req, res) => {
  const x = add(1,2)
  res.send(`Sum: ${x}`)
})

app.get('/', (req, res) => {
  res.send('Hello World!' + `sum ${add(2,3)}`)
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})